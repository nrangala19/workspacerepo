variable "hostname" {
  description = "The TFE organization to apply your changes to."
  default = "app.terraform.io"
}

variable "token" {
  description = "The TFE organization to apply your changes to."
  default = "CYJz7m6ZZgDzmQ.atlasv1.gcWYqb5cN9qUi7akFjUs9guiFb2oyptgZdfXEZyEQhh7eOuR4mT8qEhU7mKuJzMLezw"
}


variable "org" {
  description = "The TFE organization to apply your changes to."
  default = "awsrcs"
}

variable "oauth_token" {
  description = "The TFE organization to apply your changes to."
  default = "ot-XdyJyS4pe5JUa1mH"
}

variable "samplerepo" {
  description = "The TFE organization to apply your changes to."
  default = "workspacerepo"
}

provider "tfe" {
  hostname = "${var.hostname}"
  token    = "${var.token}"
}

# resource "tfe_workspace" "samplerepo" {
#   name         = "samplerepo"
#   organization = "${var.org}"
#   auto_apply   = true
#   # queue_all_runs = false
#   terraform_version = "0.12.7"

#   vcs_repo {
#     branch         = "master"
#     identifier     = "nrangala19/dev"
#     oauth_token_id = "${var.oauth_token}"
#   }
# }
# module "sentinel_policies" {
#   source = "git::https://nrangala19@bitbucket.org/nrangala19/terraform-tfe-sentinel.git"
module "sentinel" {
  source  = "app.terraform.io/awsrcs/sentinel/tfe"
  version = "~> 1.2.3"  
  t_organization = "${var.org}"
  t_workspace = "${var.samplerepo}"
  t_policies_path = "/"
  t_vcs_identifier = "nrangala19/terraform-tfe-sentinel"
  t_oauth_token_id = "${var.oauth_token}"
}
